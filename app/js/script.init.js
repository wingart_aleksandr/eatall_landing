//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/





		var owl = $(".js-slider");



			if(owl.length){
					include('plugins/owl.carousel/owl.carousel.js');
					includeCss('plugins/owl.carousel/owl.carousel.css');
			}



			function include(url){

					document.write('<script src="'+ url + '"></script>');

			}

			function includeCss(href){

					var href = '<link rel="stylesheet" media="screen" href="' + href +'">';

					if($("[href*='style.css']").length){

						$("[href*='style.css']").before(href);

					}
					else{

						$("head").append(href);

					}

			}




		$(document).ready(function(){





			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){
						owl.owlCarousel({
							items : 4,
							loop: true,
							nav: true,
							dots:true,
							responsive:{
						        0:{
						            items:1
						        },
						        768:{
						            items:2
						        },
						        992:{
						            items:3
						        },
						        1200:{
						            items:4
						        }
						    }
						});
					}

			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




		});


		$(window).load(function(){



		});




//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================

;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;

			self.accordion();
			self.tabs();
			self.navigation.init()
		},

		windowLoad: function(){

			var self = this;

			self.preloader();

		},

        /**
		**	Main navigation
		**/

		navigation: {

		    init: function () {

		    	var self = this;

		    	self.w = $(window);
		    	self.body = $('body');
		    	self.nav = $('#header');
		    	self.section = $('.section');
		    	self.sectionQt = self.section.length;

		    	self.anchorScroll();

		    	self.openCloseMenu();

		    	self.w.on('scroll',function(){

		    		self.pageScroll();
					self.sticky();

		    	});
		    	// sticky
				self.header = $('#header');

				self.sticky();

				self.w.on('resize',function(){

					self.sticky();

				});

		    },

		    anchorScroll: function(){

		    	var self = this;

		    	self.nav.on('click', "#menu a", function(event){

		    		event.preventDefault();

		    		var $this = $(this),
		    			item = $this.parent(),
		    			dataId = $this.attr('href'),
		    			offset = $(dataId).offset().top;


		    		$this.addClass('current').parent().addClass('current').siblings().removeClass('current').find('a').removeClass('current');

		    		self.scrollContent(offset);
		    		self.closeMenu();

		    	});

		    },

		    scrollContent: function(offset){

		    	var self = this;

		    	self.body.addClass('scrollContent');

		    	if(self.nav.hasClass('sticky')){
    		    	$('html,body').stop().animate({

    					scrollTop: offset - self.nav.outerHeight()

    		    	},1200,function(){

    		    		self.body.removeClass('scrollContent');

    		    	});
		    	}
		    	else{

		    		$('html,body').stop().animate({

						scrollTop: offset

			    	},1200,function(){

			    		self.body.removeClass('scrollContent');

			    	});

		    	}

		    },

		    pageScroll: function(){

		    	var self = this;

		    	if(self.body.hasClass('scrollContent'))return;

		    	self.wScroll = self.w.scrollTop();
		    	self.wHeightHalf = self.w.height()/2;

		    	for (var i = 0; i <= self.sectionQt - 1; i++) {

		    		var offset = $(self.section[i]).offset().top,
		    			heightBox = $(self.section[i]).outerHeight(),
		    			bottomOffset = $(self.section[i+1]).length ? $(self.section[i+1]).offset().top : offset + heightBox,
		    			id = $(self.section[i]).attr('id'),
		    			activItem = $('#menu').find("a[href='" + "#" + id + "']");


		    		if(self.wScroll + self.wHeightHalf > offset && self.wScroll + self.wHeightHalf < bottomOffset ){

		    			setTimeout(function(){

		    			},1200)

		    			activItem.addClass('current').parent().addClass('current').siblings().removeClass('current').find('a').removeClass('current');

		    			return false;

		    		}

		    	};

		    },

		    sticky: function(){

		    	var self = this;

		    	self.wHeight = self.w.height();
		    	self.wScroll = self.w.scrollTop();
		    	self.headerHeight = self.header.outerHeight();

		    	if(self.wScroll <= self.headerHeight){

		    		if(self.position == 'top') return false;

		    		self.position = 'top';
		    		self.header.removeClass('sticky').stop().animate({
		    			"top": 0
		    		},0);

		    	}
		    	else if(self.wScroll > self.headerHeight && self.wScroll < self.wHeight/1.5){

		    		if(self.position == 'hide') return false;

		    		if(self.position == 'top'){

		    			self.position = 'hide';
		    			self.header.addClass('sticky').stop().animate({
		    				'top': -self.headerHeight
		    			},0);

		    		}
		    		else{

		    			self.position = 'hide';
		    			self.header.stop().animate({
		    				'top': -self.headerHeight
		    			},300, function(){
		    				self.header.addClass('sticky');
		    			});

		    		}

		    	}
		    	else{

		    		if(self.position == 'sticky') return false;

		    		self.position = 'sticky';
		    		self.header.addClass('sticky');
		    		self.header.stop().animate({
		    			'top': 0
		    		});

		    	}

		    },

    		openCloseMenu: function() {

	           	var self = this;

	           	self.w = $(window);


	            $(".js-btn-menu").on('click', function () {

	                $(this).toggleClass("active");
	                $(this).siblings("#primary_nav").toggleClass('show').slideToggle("medium");

	            });

	           if(self.w.width() < 768){

	                $(document).on('click', function(event){

	                   if(!$(event.target).closest('#primary_nav, .js-btn-menu').length){

	                    	$('.js-btn-menu').removeClass('active');
	                    	$('.js-btn-menu').siblings("#primary_nav").removeClass('show').slideUp("medium");

	                   }

	              });

	            }

    		},

    		closeMenu: function(){

    			var self = this;

    			if($(window).width() < 768){
    				if($("#primary_nav").hasClass('show')){
    					$('.js-btn-menu').removeClass('active');
    					$("#primary_nav").removeClass('show').slideUp("medium");
    				}
    			}

    		}

		},







		/**
		**	Tabs
		**/

		tabs: function(){

			$('.js-tabs').each(function(){

				var $this = $(this),
					active = $this.children('.js-tabs-list').find("li.active").length ? $this.children('.js-tabs-list').find("li.active") : $this.children('.js-tabs-list').find('li:first-child').addClass('active'),
					index = active.index();

				$this.find('.js-tabs-box').children("div").eq(index).show();

			});

			$('.js-tabs-list').on('click', 'li', function(){

				var ind = $(this).index();
				$(this).addClass('active').siblings().removeClass('active');
				$(this).closest('.js-tabs')
					   .find('.js-tabs-box')
					   .children()
					   .eq(ind)
					   .addClass('active')
					   .show()
					   .siblings()
					   .removeClass('active')
					   .hide();

			});

		},


		/**
		**	Accordion
		**/

		accordion: function(){
			var self = this;

			$('.js-accordion-item').on('click', '.accordion-title', function(){
				$(this).closest('.js-accordion-item').toggleClass('active').siblings().removeClass('active');
				$(this).next('p').slideToggle(400)
				$('.js-accordion-item:not(.active)').find('p').slideUp(400);
			});

		},

		/**
		**	Preloader
		**/

		preloader: function(){

			var self = this;

			self.preloader = $('#page-preloader');
	        self.spinner   = self.preloader.find('.preloader');

		    self.spinner.fadeOut();
		    self.preloader.delay(350).fadeOut('slow');
		},

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);